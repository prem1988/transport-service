<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductMappingTable extends Migration
{
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
                //
                Schema::create('product_mapping', function (Blueprint $table) {
                        $table->increments('id');
                        $table->string('product_name');
                        $table->string('description');
                        $table->integer('size_id');
                        $table->integer('color_id');
                        $table->integer('product_id');
                        $table->integer('total_quantity');
                        $table->integer('stock_in _hand');
                        $table->string('sku_number');
                        $table->timestamps('created_at');
                });
        }
        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
                //
                Schema::drop('product_mapping');
        }
}
