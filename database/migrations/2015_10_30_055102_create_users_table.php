<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('store_location_mapping_id');
            $table->string('name',100);
            $table->text('address');
             $table->text('mobile');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->rememberToken();
            $table->string('created_by', 60);
            $table->timestamps('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
