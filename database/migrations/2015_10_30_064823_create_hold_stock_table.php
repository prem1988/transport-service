<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHoldStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('hold_stock', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('transfer_from');
            $table->string('transfer_to');
            $table->string('duration');
            $table->string('number_of_items');
            $table->string('notification_status');
            $table->date('date');
            $table->timestamps('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hold_stock');
    }
}
