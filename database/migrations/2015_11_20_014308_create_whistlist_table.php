<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWhistlistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('whistlist', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_type_id');
            $table->integer('store_id');
            $table->integer('store_location_mapping_id');
            $table->integer('size_id');
            $table->integer('color_id');
            $table->text('description');
            $table->string('status');
            $table->timestamps('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('whistlist');
    }
}
