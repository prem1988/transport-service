<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaintainRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maintain_request', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hold_stock_id');
            $table->integer('user_id');
            $table->integer('duration_id');
            $table->string('customer_status');            
            $table->string('status');
            $table->string('countdown_duration');
            $table->date('date');           
            $table->timestamps('created_at');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('maintain_request');
    }
}
