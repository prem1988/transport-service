<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('request_stock', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('number_of_items');
            $table->string('notification_status');
            $table->date('date');
            $table->string('description');
            $table->timestamps('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('request_stock');
    }
}
