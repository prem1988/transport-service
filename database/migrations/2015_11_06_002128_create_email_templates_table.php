<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailTemplatesTable extends Migration
{
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
                Schema::create('email_templates', function (Blueprint $table) {
                        $table->increments('id');
                        $table->string('name');
                        $table->string('value');
                        $table->timestamps('created_at');
                });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
                Schema::drop('email_templates');  
        }
}
