<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('product_type_id');
                $table->integer('product_sub_type_id');
                $table->integer('store_location_mapping_id');
                $table->integer('brand_id');
                $table->string('product_name');
                $table->string('description');
                $table->string('sku_number');
                $table->timestamps('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product');
    }
}
