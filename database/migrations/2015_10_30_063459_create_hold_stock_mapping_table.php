<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHoldStockMappingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hold_stock_mapping', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('hold_stock_id');
                $table->integer('product_id');
                $table->integer('product_mapping_id');
                $table->string('sku_number');
                $table->string('product_name');
                $table->integer('quantity');
                $table->timestamps('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hold_stock_mapping');
    }
}
