<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWhistlistTransferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('whistlist_transfer', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('whistlist_id');
            $table->integer('store_id');
            $table->integer('suburb_id');
            $table->integer('duration_id');
            $table->timestamps('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('whistlist_transfer');
    }
}
