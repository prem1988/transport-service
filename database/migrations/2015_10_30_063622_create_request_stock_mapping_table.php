<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestStockMappingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('request_stock_mapping', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('request_stock_id');
                $table->integer('product_id');
                $table->integer('product_mapping_id');
                $table->string('product_name');
                $table->integer('quantity');
                $table->string('sku_number');
                $table->timestamps('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('request_stock_mapping');
    }
}
