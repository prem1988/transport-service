(function($) {

        /**
         * Remove the specified resource from storage.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        
        $('#datepairExample .date').datepicker({
                    'format': 'm/d/yyyy',
                    'autoclose': true
        });

         /**
         * Ajax call for Date Valid
         *
         * check for the time for selected date 
         * @return \Illuminate\Http\Response
         */
        $("#start_date").change(function(){
                var start_date= $($('#start_date')).val();
                var passed_data = {start_date:start_date};
               
               
                $.ajax({
                    url: '/ajax/date',
                    type: "POST",
                    data: passed_data,
                    success: function(data){
                        //loop array value 
                        if(data.length !== 0)
                        {
                                $.each(data, function(k, arg){ 
                                        $('#datepairExample .time').timepicker({
                                                'showDuration': true,
                                                'timeFormat': 'g:ia',
                                                'minTime': '10:00am',
                                                'maxTime': '10:00pm',
                                                'disableTimeRanges': [
                                                        [arg.start_time,arg.end_time],
                                                ]
                                       });
                                                //usinf date pair JS to convert time
                                $('#datepairExample').datepair();
                                        
                                }); 
                        }
                        else
                        {
                                $('#datepairExample .time').timepicker({
                                            'showDuration': true,
                                            'timeFormat': 'g:ia',
                                            'minTime': '10:00am',
                                            'maxTime': '10:00pm',
                                            'disableTimeRanges': [],
                                       });
                                       
                                               //usinf date pair JS to convert time
                                        $('#datepairExample').datepair();
                        }
                   
                    }
                });                  
        });
        

})(window.jQuery);
