/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function($) {
    
 var $validator = $("#transport-order").validate({
        rules: {
            
                customer_email: {
                        required: true,
                        email: true
                },
                customer_first_name: {
                        required: true,
                },
                customer_last_name: {
                        required: true,
                },
                customer_phone: {
                        required: true,
                        number:true,
                },
                pick_line_1: {
                        required: true,
                },
                pick_suburb: {
                        required: true,
                },
                delivery_line_1: {
                        required: true,
                },
                delivery_suburb: {
                        required: true,
                },
                start_date: {
                        required: true,
                },
                time_start: {
                        required: true,
                },
                time_end: {
                        required: true,
                }
        },

        messages: {

                customer_email: {
                        required: "Your email address must be in the format of name@domain.com",
                        //email: "Your email address must be in the format of name@domain.com"
                },
                customer_first_name: {
                        required: "Enter your first name",
                },
                customer_last_name: {
                        required: "Enter your last name",
                },
                customer_phone: {
                        required: "Enter your Mobile numer",
                        number: "Enter only number"
                },
                 pick_line_1: {
                        required: "Enter PickUp Address",
                },
                pick_suburb: {
                        required: "Enter Pickup Suburb",
                },
                delivery_line_1: {
                        required: "Enter Delivery Address",
                },
                delivery_suburb: {
                        required: "Enter Delivery Suburb",
                },
                start_date: {
                        required: "Please Choose Transport Date",
                },
                time_start: {
                        required: "Please Let Us know what time you need ",
                },
                time_end: {
                        required: "Please tel us ohw long you need our service for",
                }
        },

        highlight: function (element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        unhighlight: function (label,element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                label.parent().removeClass('help-block');
                label.remove(); 
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                } else {
                        error.insertAfter(element);
                }
       },
        success: function(label,element) {
                label.parent().removeClass('help-block');
                label.remove(); 
        }
});

})(jQuery);

