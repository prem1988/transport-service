(function($){

  /*Defining our jQuery plugin */
  
  $.fn.vq_lightbox = function(prop){

    /*/ Default parameters*/

    var options = $.extend({
                height : "250",
                width : "500",
                title:"VisualQuotes",
                description: "Description",
                top: "20%",
                left: "30%",
    },prop);

    return this.click(function(e){
                add_block_page();
                add_popup_box();
                add_styles();
                 $('body').css('overflow','hidden');
                $('.vq_lightbox_box').fadeIn();
      
    });

     function add_styles(){     
                $('.vq_lightbox_box').css({ 
                        'position':'absolute', 
                        'left':options.left,
                        'top':options.top,
                        'display':'none',
                        'height': options.height + 'px',
                        'width': options.width + 'px',
                        'box-shadow': '0px 2px 7px #292929',
                        '-moz-box-shadow': '0px 2px 7px #292929',
                        '-webkit-box-shadow': '0px 2px 7px #292929',
                        'border-radius':'10px',
                        '-moz-border-radius':'10px',
                        '-webkit-border-radius':'10px',
                        'z-index':'50',
                });
                $('.vq_lightbox_close').css({
                        'position':'absolute',
                        'top':'-15px',
                        'right':'-15px',
                        'float':'right',
                        'display':'block',
                        'height':'50px',
                        'width':'50px',
                        'z-index':"5",
                        'background': 'url(/public/assets/img/close.png) no-repeat',
                });


                /*Block page overlay*/
                var pageHeight = $(document).height();
                var pageWidth = $(window).width();

                var value4 = 10;
                var height4= (pageHeight- value4); 

                if(pageWidth < 600){
                         $(".vq_lightbox").css('height',height4);
                }


      $('.vq_lightbox_block_page').css({
                'position':'absolute',
                'top':'0',
                'left':'0',
                'background-color':'rgba(0,0,0,0.8)',
                'height':pageHeight,
                'width':pageWidth,
                'z-index':'10'
      });
      $('.vq_lightbox_inner_box').css({
                'background-color':'#fff',
                'height':(options.height - 50) + 'px',
                'width':(options.width - 50) + 'px',
                'padding':'10px',
                'margin':'15px',
                'border-radius':'10px',
                '-moz-border-radius':'10px',
                '-webkit-border-radius':'10px'
      });
    }

     function add_block_page(){
                var opacity_block = $('<div class="vq_lightbox_block_opacity"></div>');
                var block_page = $('<div class="vq_lightbox_block_page"></div>');

                $(opacity_block).appendTo('body');
                $(block_page).appendTo('body');
    }

    function add_popup_close()
    {
                $('.vq_lightbox_close').trigger("click");
    }

    function add_popup_box(){
                /*var pop_up = $('<div class="vq_lightbox_box"><a href="#" class="vq_lightbox_close"></a><div class="vq_lightbox_inner_box"><h2>' + options.title + '</h2><p>' + options.description + '</p></div></div>');*/
                var pop_up = $('<div>').attr('class', 'vq_lightbox_box').append(
                        $('<a>').attr('href', 'javascript:void(0);').attr('class', 'vq_lightbox_close')
                ).append(
                        $('<div>').attr('class', 'vq_lightbox').append(
                                options.title
                  ).append(
                                options.description
                  )
                );
                $(pop_up).appendTo('.vq_lightbox_block_page');

                $('.vq_lightbox_close').click(function(){
                        /*//$(this).parent().fadeOut().remove();*/
                        $(this).parent().fadeOut().detach();
                        /*/$('.vq_lightbox_block_page').fadeOut().remove();*/
                        $('.vq_lightbox_block_page').fadeOut().detach();
                        $('.vq_lightbox_block_opacity').fadeOut().detach();
                        $('body').css('overflow','visible');
                });
    }

    return this;
  };
  
})(jQuery);