<?php

return array(
    
                /*
	|--------------------------------------------------------------------------
	| Time Frame
	|--------------------------------------------------------------------------
	|
	| This is the default types for the Time Frame
	|
	*/

                'time' => array(
                        '10.00 Am'  => 1,
                        '10.30 Am' => 2,  
                        '11.00 Am'  => 3, 
                        '11.30 Am'  => 4,
                        '12.00 Pm' => 5,
                        '12.30 Pm' => 6,
                        '01.00 Pm' => 7,
                        '01.30 Pm' => 8,
                        '02.00 Pm' => 9, 
                        '02.30 Pm' => 10,
                        '03.00 Pm ' => 11,
                        '03.30 Pm' => 12,
                        '04.00 Pm' => 13,
                        '04.30 Pm' => 14,
                        '05.00 Pm' => 15,
                        '05.30 Pm' => 16,
                        '06.00 Pm' => 17,
                        '06.30 Pm' => 18,
                        '07.00 Pm' => 19,
                        '07.30 Pm' => 20,
                        '08.00 Pm' => 21,
                        '08.30 Pm' => 22,
                        '09.00 Pm' => 23,
                        '09.30 Pm' => 24,
                        '10.00 Pm' => 25,
                    ),

    
                'time_reverse' => array(
                        1 => '10.00 Am',
                        2 => '10.30 Am',  
                        3 => '11.00 Am', 
                        4 => '11.30 Am' ,
                        5 => '12.00 Pm',
                        6 => '12.30 Pm',
                        7 => '01.00 Pm',
                        8 =>  '01.30 Pm',
                        9 =>  '02.00 Pm', 
                        10 =>  '02.30 Pm',
                        11 =>  '03.00 Pm ',
                        12 => '03.30 Pm',
                        13 =>  '04.00 Pm',
                        14 =>  '04.30 Pm',
                        15 =>  '05.00 Pm',
                        16 =>   '05.30 Pm',
                        17 =>   '06.00 Pm',
                        18 =>   '06.30 Pm',
                        19 =>  '07.00 Pm',
                        20 =>   '07.30 Pm',
                        21 =>   '08.00 Pm',
                        22 =>   '08.30 Pm',
                        23 =>   '09.00 Pm',
                        24 =>   '09.30 Pm',
                        25 =>   '10.00 Pm',
                    ),
   
    
              );
