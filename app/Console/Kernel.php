<?php

namespace App\Console;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\HoldStockController;
use Auth;
use Input;
use Session;
use Carbon\Carbon;
use App\Http\Models\User;
use App\Http\Models\UserType;
use App\Http\Models\Location;
use App\Http\Models\Suburb;
use App\Http\Models\UserMapping;
use App\Http\Models\StoreLocationMapping;
use App\Http\Models\AccessLevel;
use App\Http\Models\Duration;
use App\Http\Models\HoldStock;
use App\Http\Models\HoldStockMapping;
use App\Http\Models\MaintainRequest;
use App\Http\Models\Transfer;
use App\Http\Models\Product;
use App\Http\Models\ProductType;
use App\Http\Models\ProductSubType;
use App\Http\Models\productMapping;
use App\Http\Models\ProductLocationMapping;
use App\Http\Libraries\Mailer;
use App\Http\Models\Notification;

class Kernel extends ConsoleKernel
{
        /**
         * The Artisan commands provided by your application.
         *
         * @var array
         */
        protected $commands = [
                \App\Console\Commands\Inspire::class,
                \App\Console\Commands\Scheduler::class,
        ];

        /**
         * Define the application's command schedule.
         *
         * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
         * @return void
         */
        protected function schedule(Schedule $schedule)
        {
                $time = '05.30';
                $schedule->command('inspire')->hourly();
                
                $schedule->command('seek:schedule')->dailyAt($time);
        }
}


