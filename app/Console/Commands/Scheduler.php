<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Auth;
use Input;
use Session;
use Carbon\Carbon;
use App\Http\Models\User;
use App\Http\Models\UserType;
use App\Http\Models\Location;
use App\Http\Models\Suburb;
use App\Http\Models\UserMapping;
use App\Http\Models\StoreLocationMapping;
use App\Http\Models\AccessLevel;
use App\Http\Models\Duration;
use App\Http\Models\HoldStock;
use App\Http\Models\HoldStockMapping;
use App\Http\Models\MaintainRequest;
use App\Http\Models\Transfer;
use App\Http\Models\Product;
use App\Http\Models\ProductType;
use App\Http\Models\ProductSubType;
use App\Http\Models\productMapping;
use App\Http\Models\ProductLocationMapping;
use App\Http\Libraries\Mailer;
use App\Http\Models\Notification;

class Scheduler extends Command
{
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
        protected $signature = 'seek:schedule';

        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Command description';

        /**
         * Create a new command instance.
         *
         * @return void
         */
        public function __construct()
        {
            parent::__construct();
        }

        /*
         *
         * @return mixed
         * 
         *  Dev: Kaji R. 
         * Created for auto update remaining days for seek stock app
         *         
         */
        public function handle()
        {
                // get maintain request all data
                $data_maintain_requests = MaintainRequest::all();

                // reduce count down by -1
                foreach ($data_maintain_requests as $data_maintain_request) {
                        
                        // get product name using hold stock map id
                        $hold_stock_mapping = HoldStockMapping::where('id','=',$data_maintain_request['hold_stock_mapping_id'])->first();
                        $product_name = $hold_stock_mapping['product_name'];
                        
                        // if maintain request countdown is 4
                        if($data_maintain_request['countdown_duration'] == 4) {

                                // update notification status to 'Expiring - 14'
                                $notifications = Notification::where('object_id','=',$data_maintain_request['hold_stock_mapping_id'])->first();
                                $notifications->type = 'holdstock|Expiring';
                                $notifications->subject = 'Expiring: '.$product_name.' is expiring on this '.$data_maintain_request['date'];
                                $notifications->status = 'Expiring';
                                $notifications->is_read = 1;
                                $notifications->save();
                        } 
                        
                         // if maintain request countdown is 1
                        if ($data_maintain_request['countdown_duration'] == 1) {
                            
                                // update notification status to 'Not Full Filled - 13'
                                $notifications = Notification::where('object_id','=',$data_maintain_request['hold_stock_mapping_id'])->first();
                                $notifications->type = 'holdstock|Not Fulfilled';
                                $notifications->subject = 'Not Fulfilled: '.$product_name.' is expiring on this '.$data_maintain_request['date'];
                                $notifications->status = 'Not Fulfilled';
                                $notifications->is_read = 1;
                                $notifications->save();
                                
                                // get hold stock id using hold stock mapping id
                                $hold_stock_mapping = HoldStockMapping::where('id','=',$data_maintain_request['hold_stock_mapping_id'])->first();
                                $hold_stock_id = $hold_stock_mapping['hold_stock_id'];
                                
                                // update hold stock status to 'Not Full Filled - 13'
                                $hold_stocks = HoldStock::where('id','=',$hold_stock_id)->first();
                                $hold_stocks->notification_status = 4;
                                $hold_stocks->save();
                                
                                // update hold stock mapping status to 'Not Full Filled - 13'
                                $hold_stock_maps = HoldStockMapping::where('id','=',$data_maintain_request['hold_stock_mapping_id'])->first();
                                $hold_stock_maps->status = 4;
                                $hold_stock_maps->save();
                                
                                // update maintain request status to 'Not Full Filled - 13'
                                $maintain_requests_by_map = MaintainRequest::where('hold_stock_mapping_id','=',$data_maintain_request['hold_stock_mapping_id'])->first();
                                $maintain_requests_by_map->status = 4;
                                $maintain_requests_by_map->save();
                                
                                // update transfer status to 'Not Full Filled - 13'
                                $transfers_by_map = Transfer::where('hold_stock_mapping_id','=',$data_maintain_request['hold_stock_mapping_id'])->first();
                                
                                if(count($transfers_by_map['id']) != 0){
                                        $transfers_by_map->status = 4;
                                        $transfers_by_map->save();
                                }
                        } 
                        
                        if($data_maintain_request['countdown_duration'] != 0){
                        
                                $new_maintain_request_countdown = ($data_maintain_request['countdown_duration'] - 1);

                                // update maintain request countdown
                                $maintain_requests = MaintainRequest::where('id','=',$data_maintain_request['id'])->first();
                                $maintain_requests->countdown_duration = $new_maintain_request_countdown;
                                $maintain_requests->save();
                        }
                }

                // get transfers data
                $data_transfers = Transfer::all();

                // reduce count down by -1
                foreach ($data_transfers as $data_transfer) {
                    
                        if($data_transfer['countdown_duration'] != 0){
                        
                                $new_transfer_countdown = ($data_transfer['countdown_duration'] - 1);

                                // update transfer countdown
                                $transfers = Transfer::where('id','=',$data_transfer['id'])->first();
                                $transfers->countdown_duration = $new_transfer_countdown;
                                $transfers->save();
                        }
                }

                // email related functions
                $get_hold_stocks = HoldStock::all();
                
                $array_hold_stock_user_ids = array();    

                foreach ($get_hold_stocks as $get_hold_stock) {
                        $array_hold_stock_user_ids[$get_hold_stock->user_id][] = $get_hold_stock->id;
                }
                
                $get_hold_stock_mappings = array();
                        
                foreach ($array_hold_stock_user_ids as $key => $value){

                        $get_hold_stock_mappings = DB::table('hold_stock_mapping')
                                                                        ->join('maintain_request', 'maintain_request.hold_stock_mapping_id', '=', 'hold_stock_mapping.id')
                                                                        ->where('maintain_request.countdown_duration', '=', 3)
                                                                        ->where('hold_stock_mapping.hold_stock_id', '=', $value)
                                                                        ->select('hold_stock_mapping.*')
                                                                        ->get();

                        $customer = User::where('id','=',$key)->first();
                        
                        if(isset($get_hold_stock_mappings) && !empty($get_hold_stock_mappings)){
                            
                                // push e-mail    
                                Mailer::scheduler_holdstock_expiring_email_to_customer($get_hold_stock_mappings, $customer);
                        }
                        
                        $get_hold_stock_mappings_val_zero = DB::table('hold_stock_mapping')
                                                                                ->join('maintain_request', 'maintain_request.hold_stock_mapping_id', '=', 'hold_stock_mapping.id')
                                                                                ->where('maintain_request.countdown_duration', '=', 1)
                                                                                ->where('hold_stock_mapping.hold_stock_id', '=', $value)
                                                                                ->select('hold_stock_mapping.*')
                                                                                ->get();
                        
                        if(isset($get_hold_stock_mappings_val_zero) && !empty($get_hold_stock_mappings_val_zero)){
                            
                                // push e-mail    
                                Mailer::scheduler_holdstock_expired_email_to_customer($get_hold_stock_mappings_val_zero, $customer);
                        }
                }
        }
}
