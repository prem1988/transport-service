<?php

namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;

class Transport extends Model
{
        /**
        * The table associated with the model.
        *
        * @var string
        */
       protected $table = 'transport';

       /**
        * The attributes that are mass assignable.
        *
        * @var array
        */
       protected $fillable = ['first_name','last_name' ,'email','phone','p_address_line_1', 'p_address_line_2', 'p_suburb', 'p_city', 'd_address_line_1', 'd_address_line_2','d_suburb','d_city','start_date','end_date','start_time','end_time'];
       
       public static function saveDataTransport($fname,$lname,$email,$phone,$pline1,$pline2,$psuburb,$pcity,$dline1,$dline2,$dsuburb,$dcity,$sdate,$edate,$stime,$etime)
        {
                $transport = new Transport;

                $transport->first_name = $fname;
                $transport->last_name = $lname;
                $transport->email = $email; 
                $transport->phone = $phone;
                $transport->p_address_line_1 = $pline1;
                $transport->p_address_line_2 = $pline2;
                $transport->p_suburb = $psuburb;
                $transport->p_city = $pcity ;
                $transport->d_address_line_1 = $dline1;
                $transport->d_address_line_2 = $dline2;
                $transport->d_suburb = $dsuburb;
                $transport->d_city = $dcity ;
                $transport->start_date = $sdate;
                $transport->end_date = $edate;
                $transport->start_time = $stime;
                $transport->end_time = $etime;
                        
                if($transport->save()) {
                        return $transport->id;
                }
        }
       
}


      