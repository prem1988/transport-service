<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


// - route for - hold-stock
Route::resource('transport', 'TransportController');
Route::get('/', array('as' => 'homepage', 'uses' => 'TransportController@index'));
Route::get('/create', array('as' => 'createOrder', 'uses' => 'TransportController@create'));
 Route::post('/ajax/date',array('as'=>'ajaxDate', 'uses'=>'TransportController@ajaxDate'));
 
 
// Globals
$app_info = new stdClass();
$app_info->site_title = 'Transport Service';
$app_info->site_url = URL::route('homepage');

