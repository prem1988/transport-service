<?php

namespace App\Http\Controllers\Auth;

use App\Http\Models\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Libraries\Mailer;
use Mail;
use DB;
use Auth;
use Input;
use Session;
use Hash;

class ForgotPasswordController extends Controller
{
    
    
    
     public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }
    
    
    public function getEmail()
     { 
        return view('auth/password');
     }    
     
     public function postEmail()
             
     {
       $email = Input::get('email'); 
       $customer  = User::where('email','=',$email)->first();
       
       if(!empty($customer)){
         
           Mailer::forgotEmailCustomer($customer);
           Session::flash('success', 'Password Reset link send successfully!');
           return view('auth/password')->with('success', 'Password Reset link send successfully!');
                   
       }
        else {
            Session::flash('success', 'Email Id not available!');
            return view('auth/password')->with('success', 'Email Id not available!');
        }
       
       
       
     }
     
     
     public function getReset()
     {
        
         return view('auth/reset');
     }
     
     
     public function postReset()
     {
         
            $email                  = Input::get('email');    
            $password               = Input::get('password');
            $password_confirmation  = Input::get('password_confirmation');
            
            $userDetails             =   User::where('email','=',$email)->first();
           
            if($password == $password_confirmation)
            {
            $userDetails->password          = Hash::make(Input::get('password')); 
            }
            $userDetails->save();
            
            return redirect('auth/login');
     }
}
