<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Auth;
use Input;
use Log;
use Session;
use Carbon\Carbon;
use App\Http\Models\Location;
use App\Http\Models\Suburb;
use App\Http\Models\StoreLocationMapping;

class TransportController extends Controller
{
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
                //create new array 
                $args = array();
                //get all data from transport table
                $args['orders'] = \App\Http\Models\Transport::all();
                
                //retrun to page
                return view('transport/index', compact('args'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
                //return to page
                return view('transport/create', compact('args'));
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            //
                //get information about customer
                $fname = input::get('customer_email'); 
                $lname = input::get('customer_first_name'); 
                $email = input::get('customer_last_name'); 
                $phone = input::get('customer_phone'); 
                
                //get pick up information 
                $pline1 = input::get('pick_line_1'); 
                $pline2 = input::get('pick_line_2'); 
                $psuburb = input::get('pick_suburb'); 
                $pcity = input::get('pick_city'); 
                
                //get deliver information
                $dline1 = input::get('delivery_line_1'); 
                $dline2 = input::get('delivery_line_2'); 
                $dsuburb = input::get('delivery_suburb'); 
                $dcity = input::get('delivery_city'); 
                
                //select date and time
                $sdate = input::get('start_date'); 
                $edate = input::get('end_date'); 
                $stime = input::get('time_start'); 
                $etime = input::get('time_end'); 

                //Save Data into table
                \App\Http\Models\Transport::saveDataTransport($fname, $lname, $email, $phone, $pline1, $pline2, $psuburb, $pcity, $dline1, $dline2, $dsuburb, $dcity, $sdate, $edate, $stime, $etime);
                
                //rirect to index page with success messge
                return redirect("/")->with('alert', 'Thank you, your Order has successfully been created.');
        }
        
        
        /**
         *Ajax call for date Change
         *
         */
        public function ajaxDate()
        {   
                //get data after choosen
                $date = input::get('start_date'); 
                
                //select data equals to choose date
                $transport_date = \App\Http\Models\Transport::where('start_date','=',$date)->get();  

                  return $transport_date;
        }


        /**
         * Display the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            //
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            //
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            //
        }
}
