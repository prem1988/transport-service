@extends('layouts.master')

@section('class', 'Transport_Services' )

@section('content')

<h2 class="color1  text-align-center">Transport Services</h2>

        <div class="container-fluid container-fixed bg-white bg-white">

        <!-- START PANEL -->
        <div class="panel panel-transparent">

                <div class="panel-heading">
                        <div class="panel-title">
                            <div style="text-align: left;">
                                                </a>
                                </div>
                        </div>
                        <div class="col-xs-12">
                                <a href = "{{ URL::to('create') }}">
                                <button id="show-modal" class="btn btn-primary btn-cons"><i class="fa fa-plus"></i> New Booking</button>
                                </a>
                        </div>
                </div>
                <div class="pull-right" id="div_search_from_hold_stock">
                       <div class="col-xs-12">
                               <input type="text" name="search_from_hold_stock" id="search_from_hold_stock" class="form-control" placeholder="Search the keyword" >  
                       </div>
               </div>

                <div class="clearfix"></div>
       </div>
            
                <hr>
            
        <div class="panel-body">
                <div class="row">

                        <div class="col-sm-12">

                                <div class="panel panel-transparent ">
                                                <!-- Nav tabs -->
                                                <ul class="nav nav-tabs nav-tabs-simple">
                                                        <li class="active">
                                                                <a data-toggle="tab" href="#order-fillup1"><span>CURRENT ORDERS</span></a>
                                                        </li>
                                                </ul>
                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                        @include('transport.allList')
                                                </div>
                                </div>
                        </div>
                </div>
        </div>
</div>
<!-- END PANEL -->



@stop