@extends('layouts.master')

@section('class', 'Transport_Services' )

@section('content')

<div class="row">
        <div class="col-sm-4" style="margin-top: 15px;">
                <a href = "{{ URL::to('/') }}" class="btn btn-info"><i class="pg-arrow_left_line_alt"></i> Back</a>
        </div>
        <div class="col-sm-4">
                <h2 class="color1  text-align-center">NEW BOOKING</h2>
        </div>
</div>

<div class="row">
    <div class="col-md-12">

        <!-- START PANEL -->
        <div class="panel panel-default">

            <div class="panel-body">

                {!! Form::open(['url'=>'transport','class'=>'form-group', 'id'=>'transport-order']) !!}

                <div class="row customer_details_class">
                        <div class="col-md-12">
                            <fieldset>
                                <legend><h4><span class="semi-bold">CUSTOMER</span> DETAILS</h4></legend>

                                  <div class="row">
                                        <div class="col-sm-6">
                                                <div class="form-group form-group-default required">
                                                        <label>email</label>
                                                        <input type="email" id="customer_email"  autocomplete="false" name="customer_email" class="form-control" placeholder="enter your email address">
                                                </div>
                                        </div>

                                        <div class="col-sm-6">
                                                <div class="form-group form-group-default required">
                                                        <label>first name</label>
                                                        {!! Form::text('customer_first_name',null,['class'=>'form-control','autocomplete'=>'false', 'placeholder'=>'', 'id' => 'customer_first_name']) !!}
                                                </div>
                                        </div>
                                </div>

                                <div class="row" style="margin-top: 10px;">
                                        
                                        <div class="col-sm-6">
                                                <div class="form-group form-group-default required">
                                                        <label>last name</label>
                                                        {!! Form::text('customer_last_name',null,['class'=>'form-control','autocomplete'=>'false', 'placeholder'=>'', 'id' => 'customer_last_name']) !!}
                                                </div>
                                        </div>
                                        <div class="col-sm-6">
                                                <div class="form-group form-group-default required">
                                                        <label>phone</label>
                                                        {!! Form::text('customer_phone',null,['class'=>'form-control','autocomplete'=>'false','placeholder'=>'', 'id' => 'customer_phone']) !!}
                                                </div>
                                        </div>
                                </div>
                            </fieldset>
                        </div>
                </div>
                
                <hr/>
                
                <div class="row Note ">
                    <h5 class="color-orange text-align-center">We do delivery only inside Auckland</h5>  
                </div>
                
                <hr/>
                
                <div class="row Pickup_details_class">
                        <div class="col-md-6">
                            <fieldset class="padding-left-right-20">
                                <legend><h4><span class="semi-bold">PICKUP</span> ADDRESS</h4></legend>

                                  <div class="row ">
                                        <div class="col-sm-6">
                                                <div class="form-group form-group-default required">
                                                        <label>Address Line 1</label>
                                                        <input type="text" id="pick_line_1"  autocomplete="false" name="pick_line_1" class="form-control" placeholder="enter your address">
                                                </div>
                                        </div>

                                        <div class="col-sm-6">
                                                <div class="form-group form-group-default">
                                                        <label>Address Line 2</label>
                                                        {!! Form::text('pick_line_2',null,['class'=>'form-control','autocomplete'=>'false', 'placeholder'=>'enter your address', 'id' => 'pick_line_2']) !!}
                                                </div>
                                        </div>
                                </div>

                                <div class="row " style="margin-top: 10px;">
                                        
                                        <div class="col-sm-6">
                                                <div class="form-group form-group-default required">
                                                        <label>Suburb</label>
                                                        {!! Form::text('pick_suburb',null,['class'=>'form-control','autocomplete'=>'false', 'placeholder'=>'', 'id' => 'pick_suburb']) !!}
                                                </div>
                                        </div>
                                         <div class="col-sm-6">
                                                <div class="form-group form-group-default required">
                                                        <label>City</label>
                                                        {!! Form::text('pick_city',null,['class'=>'form-control','autocomplete'=>'false', 'Value'=>'Auckland', 'id' => 'pick_city']) !!}
                                                </div>
                                        </div>

                                </div>
                            </fieldset>
                        </div>
                          <div class="col-md-6">
                                <fieldset class="padding-left-right-20">
                                <legend><h4><span class="semi-bold">DELIVERY</span> ADDRESS</h4></legend>

                                <div class="row ">
                                        <div class="col-sm-6">
                                                <div class="form-group form-group-default required">
                                                        <label>Address Line 1</label>
                                                        <input type="text" id="delivery_line_1"  autocomplete="false" name="delivery_line_1" class="form-control" placeholder="enter your address">
                                                </div>
                                        </div>

                                        <div class="col-sm-6">
                                                <div class="form-group form-group-default">
                                                        <label>Address Line 2</label>
                                                        {!! Form::text('delivery_line_2',null,['class'=>'form-control','autocomplete'=>'false', 'placeholder'=>'enter your address', 'id' => 'delivery_line_2']) !!}
                                                </div>
                                        </div>
                                </div>

                                <div class="row " style="margin-top: 10px;">
                                        
                                        <div class="col-sm-6">
                                                <div class="form-group form-group-default required">
                                                        <label>Suburb</label>
                                                        {!! Form::text('delivery_suburb',null,['class'=>'form-control','autocomplete'=>'false', 'placeholder'=>'', 'id' => 'delivery_suburb']) !!}
                                                </div>
                                        </div>
                                         <div class="col-sm-6">
                                                <div class="form-group form-group-default required">
                                                        <label>City</label>
                                                        {!! Form::text('delivery_city',null,['class'=>'form-control','autocomplete'=>'false', 'Value'=>'Auckland', 'id' => 'delivery_city']) !!}
                                                </div>
                                        </div>

                                </div>

                            </fieldset>
                        </div>
                </div>
                
                 <div class="row customer_details_class"  id="datepairExample">
                        <div class="col-md-12">
                            <fieldset>
                                <legend><h4><span class="semi-bold">OTHER</span> DETAILS</h4></legend>

                                  <div class="row">
                                        <div class="col-sm-4">
                                                <div class="form-group form-group-default required">
                                                        <label>Date</label>
                                                        <input type="text" id="start_date"  autocomplete="false" name="start_date" class="form-control date start" placeholder="Select Date">
                                                </div>
                                        </div>

                                        <div class="col-sm-4">
                                                <div class="form-group form-group-default required">
                                                        <label>Start Time</label>
                                                        {!! Form::text('time_start',null,['class'=>'form-control time start','autocomplete'=>'false', 'placeholder'=>'Start Time', 'id' => 'time_start']) !!}
                                                </div>
                                        </div>
                                        <div class="col-sm-4">
                                                <div class="form-group form-group-default required">
                                                        <label>End Time</label>
                                                        {!! Form::text('time_end',null,['class'=>'form-control time end','autocomplete'=>'false', 'placeholder'=>'End Time', 'id' => 'time_end']) !!}
                                                </div>
                                        </div>
                                        <div class="col-sm-12 display-none ">
                                                <div class="form-group form-group-default">
                                                        <label>Date</label>
                                                        <input type="text" id="end_date"  autocomplete="false" name="end_date" class="form-control date end " placeholder="Select Date">
                                                </div>
                                        </div>
                                </div>
   
                            </fieldset>
                        </div>
                </div>
                
                <div class="row">
                        <div class="col-sm-6">
                            {!! Form::submit('Save',['class'=>'btn btn-success']) !!}
                            <a href = "{{ URL::to('hold-stock') }}" class="btn btn-default">Cancel</a>
                        </div>
                </div>
        </div>        
        </div>
        <!-- END PANEL -->
    </div>
</div>

@stop

