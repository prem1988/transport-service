<div class="tab-pane active" id="order-fillup1">
        <div class="row">
                <div class="col-md-12">
                        <div class="table-responsive">
                                <table class="table table-hover demo-table-search dataTable no-footer" id="tableWithDynamicRows1" role="grid" aria-describedby="tableWithDynamicRows_info">

                                        @if(isset($args['orders']) && !empty($args['orders']))
                                                <thead>
                                                        <tr>
                                                                <th style="width: 10%;">Date</th>
                                                                <th style="width: 5%;">Action</th>
                                                                <th style="width: 20%;">Customer <br />  Contact Method</th>
                                                                 <th style="width: 20%;">Date</th>
                                                                  <th style="width: 20%;">Start Time <br />  End TIme</th>
                                                                <!--<th>Notification</th>-->
                                                        </tr>
                                                </thead>

                                                <tbody>
                                                        @foreach($args['orders'] as $key => $all_new_orders)
                                                                <tr>
                                                                        <td>{{Carbon::createFromFormat('Y-m-d H:i:s', $all_new_orders->created_at)->format('d-M-y')}} </td>
                                                                        <td>
                                                                                <a href="#">
                                                                                    <button class="btn" data-toggle="tooltip" data-original-title="Change"><i class="fa fa-edit"></i></button>
                                                                                </a>
                                                                        </td>
                                                                        <td>
                                                                                <a href="#">
                                                                                        {{$all_new_orders->first_name}}
                                                                                </a><br />
                                                                                Contact : {{$all_new_orders->phone}}
                                                                        </td>
                                                                        <td>
                                                                                <a href="#">
                                                                                        {{$all_new_orders->start_date}}
                                                                                </a>
                                                                        </td>
                                                                        <td>
                                                                                <a href="#">
                                                                                     Start   {{$all_new_orders->start_time}}
                                                                                </a><br />
                                                                                End : {{$all_new_orders->end_time}}
                                                                        </td>
           
                                                                </tr>
                                                        @endforeach
                                                </tbody>
                                       @else
                                                <tbody>
                                                        <tr>
                                                             <td rowspan="8"><h3>Your Location Doesn't have any new Orders</h3></td>
                                                        </tr>
                                                </tbody>
                                        @endif

                                </table>

                        </div>
                </div>
        </div>
</div>
