<div class="header ">
        <!-- START MOBILE CONTROLS -->
        <!-- LEFT SIDE -->
        <div class="pull-left full-height visible-sm visible-xs">
                <!-- START ACTION BAR -->
                <div class="sm-action-bar">
                        <a href="#" class="btn-link toggle-sidebar" data-toggle="sidebar">
                                <span class="icon-set menu-hambuger"></span>
                        </a>
                </div>
                <!-- END ACTION BAR -->
        </div>
        <!-- RIGHT SIDE -->
        <div class="pull-right full-height visible-sm visible-xs">
                <!-- START ACTION BAR -->
                <div class="sm-action-bar">
                        <a href="#" class="btn-link" data-toggle="quickview" data-toggle-element="#quickview">
                                <span class="icon-set menu-hambuger-plus"></span>
                        </a>
                </div>
                <!-- END ACTION BAR -->
        </div>
        <!-- END MOBILE CONTROLS -->
        <div class=" pull-left sm-table">
                <div class="header-inner">
                        <div class="brand inline">
                                <img src="" alt="logo" 
                                        data-src="" 
                                        data-src-retina="" 
                                        width="150" 
                                        height="45">
                        </div>
                        <!-- START NOTIFICATION LIST -->
                        <ul class="notification-list no-margin hidden-sm hidden-xs b-grey b-l b-r no-style p-l-30 p-r-20">
                     
                                <li class="p-r-15 inline">
                                         
                                </li>
                        </ul>
                        <div class="pull-right p-r-10 p-t-10 fs-16 font-heading padding-left-10">
                                <i class="fa fa-fw fa-user"></i> <span class="semi-bold"><strong> | </strong></span> <span class="semi-bold"><strong >Transport </strong>  <span class="color-orange">Services</span></span>
                        </div>
                        <!-- END NOTIFICATIONS LIST -->
                        <a href="#" class="search-link" data-toggle="search"><i class="pg-search"></i>Type anywhere to <span class="bold">search</span></a> 
                </div>
        </div>
         <div class=" pull-right">
                <div class="header-inner">
                        <a href="#"  class="btn-link margin-left-10" >| <i class="fa fa-sign-out fa-fw fa-lg fa-2x  margin-left-5 margin-top-10"></i></a>
                </div>
        </div>
        <div class=" pull-right">
                <!-- START User Info-->
                <div class="visible-lg visible-md m-t-10">
                        <div class="pull-left p-r-10 p-t-10 fs-16 font-heading">
                                <span class="semi-bold"></span>
                        </div>
                        <div class="thumbnail-wrapper d32 circular inline m-t-5">
                                <img src="" alt="" data-src="" data-src-retina="" width="32" height="32">
                        </div>
                </div>
          <!-- END User Info-->
        </div>
</div>
      <!-- END HEADER -->