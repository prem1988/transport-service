<script>
function defaultFor(arg, val) { return typeof arg !== 'undefined' ? arg : val; }
</script>

{!!Html::script('//cdn.jsdelivr.net/angularjs/1.3.14/angular.min.js') !!}
{!!Html::script('//cdn.jsdelivr.net/algoliasearch/3/algoliasearch.angular.min.js') !!}

<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
{!!Html::script(asset('assets/plugins/pace/pace.min.js'))!!}
{!!Html::script('//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js')!!}
<!-- 
        <script>
                if (!window.jQuery) {
                        document.write('{!!Html::script(asset("assets/plugins/jquery/jquery-1.8.3.min.js"))!!}');
                }
        </script>
-->
{!!Html::script(asset('assets/plugins/modernizr.custom.js'))!!}
{!!Html::script('//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js')!!}
<!-- 
        <script>
                if (!window.jQuery.ui) {
                        document.write('{!!Html::script(asset("assets/plugins/jquery-ui/jquery-ui.min.js"))!!}');
                }
        </script>
-->
<script>
        'use strict';
        $.ajaxSetup({
                headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
        });
</script>
{!!Html::script(asset('assets/plugins/boostrapv3/js/bootstrap.min.js'))!!}
{!!Html::script(asset('assets/plugins/jquery/jquery-easy.js'))!!}
{!!Html::script(asset('assets/plugins/jquery-unveil/jquery.unveil.min.js'))!!}
{!!Html::script(asset('assets/plugins/jquery-bez/jquery.bez.min.js'))!!}
{!!Html::script(asset('assets/plugins/jquery-ios-list/jquery.ioslist.min.js'))!!}
{!!Html::script(asset('assets/plugins/jquery-actual/jquery.actual.min.js'))!!}
{!!Html::script(asset('assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js'))!!}
{!!Html::script(asset('assets/plugins/bootstrap-select2/select2.min.js'))!!}
{!!Html::script(asset('assets/plugins/classie/classie.js'))!!}
{!!Html::script(asset('assets/plugins/switchery/js/switchery.min.js'))!!}
{!!Html::script(asset('assets/plugins/nvd3/lib/d3.v3.js'))!!}
{!!Html::script(asset('assets/plugins/nvd3/nv.d3.min.js'))!!}
{!!Html::script(asset('assets/plugins/nvd3/src/utils.js'))!!}
{!!Html::script(asset('assets/plugins/nvd3/src/tooltip.js'))!!}
{!!Html::script(asset('assets/plugins/nvd3/src/interactiveLayer.js'))!!}
{!!Html::script(asset('assets/plugins/nvd3/src/models/axis.js'))!!}
{!!Html::script(asset('assets/plugins/nvd3/src/models/line.js'))!!}
{!!Html::script(asset('assets/plugins/nvd3/src/models/lineWithFocusChart.js'))!!}
{!!Html::script(asset('assets/plugins/mapplic/js/hammer.js'))!!}
{!!Html::script(asset('assets/plugins/mapplic/js/jquery.mousewheel.js'))!!}
{!!Html::script(asset('assets/plugins/mapplic/js/mapplic.js'))!!}
{!!Html::script(asset('assets/plugins/rickshaw/rickshaw.min.js'))!!}
{!!Html::script(asset('assets/plugins/jquery-metrojs/MetroJs.min.js'))!!}
{!!Html::script(asset('assets/plugins/jquery-sparkline/jquery.sparkline.min.js'))!!}
{!!Html::script(asset('assets/plugins/skycons/skycons.js'))!!}
{!!Html::script(asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'))!!}
{!!Html::script(asset('assets/plugins/jquery-validation/js/jquery.validate.min.js'))!!}
{!!Html::script(asset('assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js'))!!}
{!!Html::script(asset('assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js'))!!}
{!!Html::script(asset('assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js'))!!}
{!!Html::script(asset('assets/plugins/datatables-responsive/js/datatables.responsive.js'))!!}
{!!Html::script(asset('assets/plugins/datatables-responsive/js/lodash.min.js'))!!}
{!!Html::script(asset('assets/plugins/lightbox.js'))!!}
{!!Html::script('http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.js')!!}
<!-- END VENDOR JS -->

<!-- BEGIN TIME PICKER TEMPLATE JS -->
{!!Html::script(asset('js/lib/jquery.timepicker.js'))!!}
{!!Html::script(asset('js/lib/bootstrap-datepicker.js'))!!}
{!!Html::script(asset('js/lib/site.js'))!!}
{!!Html::script('http://jonthornton.github.io/Datepair.js/dist/datepair.js')!!}
{!!Html::script('http://jonthornton.github.io/Datepair.js/dist/jquery.datepair.js')!!}
<!-- END TIME PICKER JS -->


<!-- BEGIN CORE TEMPLATE JS -->
{!!Html::script(asset('pages/js/pages.min.js'))!!}
<!-- END CORE TEMPLATE JS -->

<!-- BEGIN PAGE LEVEL JS -->
{!!Html::script(asset('assets/js/form_elements.js'))!!}
{!!Html::script(asset('assets/js/datatables.js'))!!}
{!!Html::script(asset('assets/js/notifications.js'))!!}
{!!Html::script(asset('assets/js/dashboard.js'))!!}
{!!Html::script(asset('assets/js/scripts.js'))!!}
<!-- END PAGE LEVEL JS -->

<!-- BEGIN SEEK JS -->
{!!Html::script(asset('js/transport_validate.js'))!!}
{!!Html::script(asset('js/transport_custom.js'))!!}

<!-- END SEEK JS -->
