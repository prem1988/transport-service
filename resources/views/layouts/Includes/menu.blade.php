<!-- header.blade.php -->

<nav class="page-sidebar" data-pages="sidebar">
        <!-- BEGIN SIDEBAR MENU HEADER-->
        <div class="sidebar-header">
                <img src="" alt="logo" class="brand" data-src="" data-src-retina="" width="190" height="50">
                
                <div class="sidebar-header-controls" style="padding-left: 170px; margin-top: 5px;">
                        <button type="button" class="btn btn-link visible-lg-inline" data-toggle-pin="sidebar"><i class="fa fs-12"></i></button>
                </div>
                
        </div>
        <!-- END SIDEBAR MENU HEADER-->
        <!-- START SIDEBAR MENU -->
      <div class="sidebar-menu">
                <!-- BEGIN SIDEBAR MENU ITEMS-->
                <ul class="menu-items">
                        <li class="m-t-30">
                                <a href="#" class="detailed padding-top-10">
                                        <span class="title">Dashboard</span>
                                </a>
                                <span class="icon-thumbnail {{ (Request::is('/') ? 'bg-success' : '') }}"><i class="fa  fa-fw fa-lg fa-2x fa-home"></i></span>
                        </li>

                        <li>
                                <a href="#">
                                        <span class="title">Create</span>
                                </a>
                                        <span class="icon-thumbnail" ><i class="fa  fa-fw fa-lg fa-2x fa-cubes"></i></span>
                        </li>

                </ul>
        <div class="clearfix"></div>
      </div>
      <!-- END SIDEBAR MENU -->
</nav>
<!-- END SIDEBAR -->
