
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if lt IE 7 ]><html class="ie ie6" lang="en" xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en" xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en" xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
 <html lang="en-us"  xmlns="http://www.w3.org/1999/xhtml" ng-App="transport"><!--<![endif]-->

        <head>
                        @include('layouts.Includes.metatag')   
                        @yield('og_metadata')
                        <title>Transport Service : @yield('title') </title>
                        @include('layouts.Includes.style')   
        </head>
     
        <body ng-controller="SearchCtrl" data-app-url="" class="@yield('title') fixed-header dashboard @yield('class') loading pace-done sidebar-visible menu-pin  ">

                <!-- BEGIN SIDEBPANEL-->
                        @include('layouts.Includes.menu')
                <!-- END SIDEBPANEL-->    

                <!-- START PAGE-CONTAINER -->
                <div class="page-container">

                        <!--  START HEADER -->
                                @include('layouts.Includes.header')
                        <!--  END HEADER -->

                        <!-- START PAGE CONTENT WRAPPER -->
                        <div class="page-content-wrapper">
                                <!-- START PAGE CONTENT -->
                                <div class="content sm-gutter">
                                        <!-- START CONTAINER FLUID -->
                                                <div class="container-fluid padding-25 sm-padding-10">
                                                        <div class="row">
                                                                <div class="col-md-6 col-xlg-5">
                                                                        <div class="row">
                                                                                @if(Session::has('error_msg'))
                                                                                        <div class="row">
                                                                                                <div class="alert alert-danger alert-dismissable">
                                                                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                                                                        @if (is_array(Session::get('error_msg')))
                                                                                                                @if (count(Session::get('error_msg')) > 1)
                                                                                                                        <strong>Oops. We had some errors, please review them</strong>
                                                                                                                @elseif (count(Session::get('error_msg')) == 1)
                                                                                                                        <strong>Oops. We have an error, please review it</strong>
                                                                                                                @endif
                                                                                                                <ul>
                                                                                                                        @foreach(Session::get('error_msg') as $error)
                                                                                                                                <li>{{ $error }}</li>
                                                                                                                        @endforeach
                                                                                                                </ul>
                                                                                                        @elseif (is_string(Session::get('error_msg')))
                                                                                                        <strong>Oops. We have an error, please review it</strong>
                                                                                                                <ul>
                                                                                                                        <li>{{ Session::get('error_msg') }}</li>
                                                                                                                </ul>
                                                                                                        @endif
                                                                                                </div>
                                                                                        </div>
                                                                                @endif

                                                                                @if(Session::has('message'))
                                                                                        <div class="row">
                                                                                                <div class="alert alert-info alert-dismissable">
                                                                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                                                                        {{ Session::get('message') }}
                                                                                                </div>
                                                                                        </div>
                                                                                @endif

                                                                                @if(Session::has('alert'))
                                                                                        <div class="row">
                                                                                                <div class="alert alert-success alert-dismissable text-align-center ">
                                                                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                                                                        {{ Session::get('alert') }}
                                                                                                </div>
                                                                                        </div>
                                                                                @endif

                                                                                @if(Session::has('error'))
                                                                                            <div class="row">
                                                                                               <div class="alert alert-warning alert-dismissable">
                                                                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                                                                        <strong>Oops. We had some errors, please review them</strong>
                                                                                                        <ul>
                                                                                                                @foreach(Session::get('error') as $error)
                                                                                                                        <li>{{ $error }}</li>
                                                                                                                @endforeach
                                                                                                        </ul>
                                                                                                </div>
                                                                                    </div>
                                                                                @endif
                                                                                @if($errors->any())
                                                                                        <div class="row">
                                                                                                <div class="alert alert-danger alert-dismissable">
                                                                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                                                                        @if (count($errors->all()) > 1)
                                                                                                                <strong>Oops. We had some errors, please review them</strong>
                                                                                                        @elseif (count($errors->all()) == 1)
                                                                                                                <strong>Oops. We have an error, please review it</strong>
                                                                                                        @endif
                                                                                                        <ul>
                                                                                                                @foreach($errors->all() as $error)
                                                                                                                        <li>{{ $error }}</li>
                                                                                                                @endforeach
                                                                                                        </ul>
                                                                                               </div>
                                                                                         </div>
                                                                                @endif
                                                                        </div>
                                                                </div>
                                                        </div>
                                                        <!-- START CONTENT PAGES -->
                                                                @yield('content')
                                                        <!-- END CONTENT PAGES T -->
                                                </div>
                                        <!-- END CONTAINER FLUID -->
                                </div>
                         <!-- END PAGE CONTENT -->
                         
                        <!-- START COPYRIGHT -->
                                @include('layouts.Includes.footer')
                        <!-- END COPYRIGHT -->
                        
                        </div>
                        <!-- END PAGE CONTENT WRAPPER -->
                </div>
                <!-- END PAGE CONTAINER -->

                <!-- START SCRIPT -->
                        @include('layouts.Includes.script')
                <!--END SCRIPT -->
        </body>

</html>